<?php
/**
 * English language file for jsoneditor plugin
 *
 * @author Janez Paternoster <janez.paternoster@siol.net>
 */

// keys need to match the config setting name

$lang['options'] = 'Default options for the JSON Editor in JSON format. See https://github.com/json-editor/json-editor#options';
