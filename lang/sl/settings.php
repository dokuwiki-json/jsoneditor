<?php
/**
 * Slovenian language file for jsoneditor plugin
 *
 * @author Janez Paternoster <janez.paternoster@siol.net>
 */

// keys need to match the config setting name

$lang['options'] = 'Privzete opcije za JSON Editor v formatu JSON. Glej https://github.com/json-editor/json-editor#options';
